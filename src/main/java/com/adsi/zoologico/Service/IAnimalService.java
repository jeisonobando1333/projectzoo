package com.adsi.zoologico.Service;

import com.adsi.zoologico.Domain.Animal;
import com.adsi.zoologico.Repository.dto.AnimalNameGender;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IAnimalService {

    public ResponseEntity create (Animal animal);

    public Iterable<Animal> read();

    public  Animal update(Animal animal);

    public Optional<Animal> getById(String code);

    public  Integer quantityAnimals();

    public  Iterable<AnimalNameGender> getAnimalNameGender();


}
