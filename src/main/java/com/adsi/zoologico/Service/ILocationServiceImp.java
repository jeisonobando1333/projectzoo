package com.adsi.zoologico.Service;


import com.adsi.zoologico.Domain.Location;
import com.adsi.zoologico.Repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ILocationServiceImp implements ILocationService {
    @Autowired
    public LocationRepository locationRepository;

    @Override
    public Location create(Location location) {

            return locationRepository.save(location);
        }

    @Override
    public Iterable<Location> read(){
        return locationRepository.findAll();
    }


}
