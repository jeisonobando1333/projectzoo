package com.adsi.zoologico.Service;

import com.adsi.zoologico.Domain.LocationType;
import org.springframework.http.ResponseEntity;

public interface ILocationTypeService {
    public LocationType create(LocationType locationType);
    public Iterable<LocationType> read();

}
