package com.adsi.zoologico.Service;

import com.adsi.zoologico.Domain.Location;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface ILocationService {

 public Location create(Location location);

 public  Iterable<Location> read();

}
