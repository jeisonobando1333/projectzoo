package com.adsi.zoologico.Domain;

import com.adsi.zoologico.Domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Animal {

    @Id
    private  String code;
    private  String name;
    private  String race;
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToOne
    private  Location location;


}
