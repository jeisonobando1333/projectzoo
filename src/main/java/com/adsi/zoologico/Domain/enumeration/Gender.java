package com.adsi.zoologico.Domain.enumeration;

public enum Gender {
    MALE, FEMALE
}
