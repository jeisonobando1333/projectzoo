package com.adsi.zoologico.web.rest;

import com.adsi.zoologico.Domain.LocationType;
import com.adsi.zoologico.Service.ILocationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LocationTypeResource {

  @Autowired
    ILocationTypeService locationTypeService;

   @PostMapping("/location-type")
   public LocationType create(@RequestBody LocationType locationType) {

       return locationTypeService.create(locationType);
   }
   @GetMapping("/location-type")
    public Iterable<LocationType> read(){
       return locationTypeService.read();
   }
}
