package com.adsi.zoologico.web.rest;

import com.adsi.zoologico.Domain.Location;
import com.adsi.zoologico.Service.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/location")
public class LocationResource {

    @Autowired
    ILocationService locationService;

    @PostMapping("")
    public Location create(@RequestBody Location location){
        return locationService.create(location);
    }

    @GetMapping("")
    public  Iterable<Location> read(){
        return locationService.read();
    }

}
