package com.adsi.zoologico.Repository.dto;

public interface AnimalNameGender {
    public String getName();
    public  String getGender();
}
