package com.adsi.zoologico.Repository;

import com.adsi.zoologico.Domain.Animal;
import com.adsi.zoologico.Repository.dto.AnimalNameGender;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository <Animal, String> {

    @Query(value = "select count(animal) from  Animal animal")
    Integer quantityAnimals();

    @Query(value = "select animal.name as name, animal.gender as gender from Animal animal")
    Iterable<AnimalNameGender> findAnimalNameGender();
}
