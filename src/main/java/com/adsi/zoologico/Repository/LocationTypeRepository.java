package com.adsi.zoologico.Repository;

import com.adsi.zoologico.Domain.LocationType;
import org.springframework.data.repository.CrudRepository;

public interface LocationTypeRepository extends CrudRepository <LocationType, Integer> {
}
