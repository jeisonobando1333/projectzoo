package com.adsi.zoologico.Repository;

import com.adsi.zoologico.Domain.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<Location, Integer> {
}
